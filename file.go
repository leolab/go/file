package file

import (
	"errors"
	"io"
	"os"
)

type File struct {
	Name string
	f    *os.File
}

func NewFile(name string) (f *File, e error) {
	f = &File{
		Name: name,
	}
	f.f, e = os.OpenFile(name, os.O_CREATE|os.O_APPEND, 0777)
	if e != nil {
		return nil, e
	}
	return f, nil
}

func (f *File) ReadAll() ([]byte, error) {
	return io.ReadAll(f.f)
}

func (f *File) Write(data []byte) error {
	return errors.New("Not implemented")
}

func (f *File) Append(data []byte) error {
	return errors.New("Not implemented")
}

func (f *File) ReadJSON() (data interface{}, err error) {
	return nil, errors.New("Not implemented")
}

func (f *File) WriteJSON(data interface{}) error {
	return errors.New("Not implemetned")
}

func (f *File) ReadString() (string, error) {
	return "", errors.New("Not implemented")
}
