package file

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

var (
	ErrFileNotFound    error = errors.New("file not found")
	ErrFileReadError   error = errors.New("file read error")
	ErrFileWriteError  error = errors.New("file write error")
	ErrFileDeleteError error = errors.New("file delete error")
	ErrPathCreateError error = errors.New("path create error")
	ErrPathDeleteError error = errors.New("path delete error")
)

func GetCWD() string {
	wd, e := os.Getwd()
	if e != nil {
		return ""
	}
	return wd
}

func GetBinName() string {
	bn, e := os.Executable()
	if e != nil {
		//errs.RaiseError(ErrFileError, e.Error())
		return ""
	}
	return filepath.Base(bn)
}

func GetBinDir() string {
	bn, e := os.Executable()
	if e != nil {
		//errs.RaiseError(ErrFileError, e.Error())
		return ""
	}
	return filepath.Dir(bn)
}

func GetHome() string {
	u, e := user.Current()
	if e != nil {
		//errs.RaiseError(ErrFileError, e.Error())
		return ""
	}
	return u.HomeDir
}

func RealPath(path string) string {
	if len(path) < 1 {
		return GetCWD()
	}
	switch path[0] {
	case '.':
		return GetBinDir() + "/" + strings.TrimLeft(path, "./")
	case '~':
		return GetHome() + "/" + strings.TrimLeft(path, "~/")
	case '/':
		return path
	default:
		return GetCWD() + "/" + path
	}
}

func Ext(file string) string {
	fa := strings.Split(file, ".")
	if len(fa) < 2 {
		return ""
	}
	return fa[len(fa)-1]
}

func Exists(file string) bool {
	if _, e := os.Stat(RealPath(file)); os.IsNotExist(e) {
		return false
	}
	return true
}

func Load(file string) ([]byte, error) {
	if !Exists(file) {
		return nil, ErrFileNotFound
	}
	ret, e := ioutil.ReadFile(RealPath(file))
	if e != nil {
		return nil, ErrFileReadError
	}
	return ret, nil
}

func LoadJSON(file string) (data interface{}, e error) {
	src, e := Load(file)
	if e != nil {
		return nil, e
	}
	e = json.Unmarshal(src, &data)
	if e != nil {
		return nil, e
	}
	return data, nil
}

func Save(file string, data []byte) error {
	fName := RealPath(file)
	fPath := filepath.Dir(fName)
	if e := os.MkdirAll(fPath, 0775); e != nil {
		return ErrPathCreateError
	}
	if e := ioutil.WriteFile(fName, data, 0664); e != nil {
		return ErrFileWriteError
	}
	return nil
}

func SaveJSON(file string, data interface{}) error {
	jsonData, e := json.Marshal(data)
	if e != nil {
		return e
	}
	return Save(file, jsonData)
}

func Remove(file string) error {
	if e := os.Remove(RealPath(file)); e != nil {
		return ErrFileDeleteError
	}
	return nil
}
